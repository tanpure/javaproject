package com.nomination.model;

public class AssignTaskDataPojo {
	private int acNo;
	private int partNo;
	private int epId;
	private int i_appstatus;
	private int id;
	private String jrName;
	private String jrMobNo;
	private String jrRole;
	private String referance_uId;
	private int distId;
	private int localBodyId;
	private String localBodyName;
	private int localBodyType;
	private String divName;
	private String electoralCollegeName;
	private int wardNo;
	private String assignedWorkDetails;
	private int status;
	private String createdBy;
	private String modifiedBy;
	private String createdDate;
	private String modifiedDate;
	
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getJrName() {
		return jrName;
	}
	public void setJrName(String jrName) {
		this.jrName = jrName;
	}
	public String getJrMobNo() {
		return jrMobNo;
	}
	public void setJrMobNo(String jrMobNo) {
		this.jrMobNo = jrMobNo;
	}
	public String getJrRole() {
		return jrRole;
	}
	public void setJrRole(String jrRole) {
		this.jrRole = jrRole;
	}
	public String getReferance_uId() {
		return referance_uId;
	}
	public void setReferance_uId(String referance_uId) {
		this.referance_uId = referance_uId;
	}
	public int getDistId() {
		return distId;
	}
	public void setDistId(int distId) {
		this.distId = distId;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public String getLocalBodyName() {
		return localBodyName;
	}
	public void setLocalBodyName(String localBodyName) {
		this.localBodyName = localBodyName;
	}
	public int getLocalBodyType() {
		return localBodyType;
	}
	public void setLocalBodyType(int localBodyType) {
		this.localBodyType = localBodyType;
	}
	public String getDivName() {
		return divName;
	}
	public void setDivName(String divName) {
		this.divName = divName;
	}
	public String getElectoralCollegeName() {
		return electoralCollegeName;
	}
	public void setElectoralCollegeName(String electoralCollegeName) {
		this.electoralCollegeName = electoralCollegeName;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public String getAssignedWorkDetails() {
		return assignedWorkDetails;
	}
	public void setAssignedWorkDetails(String assignedWorkDetails) {
		this.assignedWorkDetails = assignedWorkDetails;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public int getAcNo() {
		return acNo;
	}
	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}
	public int getPartNo() {
		return partNo;
	}
	public void setPartNo(int partNo) {
		this.partNo = partNo;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getI_appstatus() {
		return i_appstatus;
	}
	public void setI_appstatus(int i_appstatus) {
		this.i_appstatus = i_appstatus;
	}
	

}
