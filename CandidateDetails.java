package com.nomination.model;

import java.io.Serializable;

public class CandidateDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    private String uId;
    private int affId;
    private String electionProgramName;
    private String localBodyId;
    private String localBodyName;
    private String dateOfBirth;
    private String age;
    private String gender;
    private String occupation;
    private String address;
    private String emailId;
    private String mobileNo;
    private String wardNo;
    private String wardName;
    private String category;
    private String caste;
    private String castCertIssuingAuthority;
    private String serialNo;
    private String dateOfValidation;
    private String receiptNo;
    private String isMarried;
    private String createdBy;
    private String modifiedBy;
    private int isSubmitted;
    private int isAprrove;
    private int isStatus;
    private String token;
	private String dateOfCertificateIssue;
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public int getAffId() {
		return affId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public String getElectionProgramName() {
		return electionProgramName;
	}
	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}
	public String getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public String getLocalBodyName() {
		return localBodyName;
	}
	public void setLocalBodyName(String localBodyName) {
		this.localBodyName = localBodyName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getWardNo() {
		return wardNo;
	}
	public void setWardNo(String wardNo) {
		this.wardNo = wardNo;
	}
	public String getWardName() {
		return wardName;
	}
	public void setWardName(String wardName) {
		this.wardName = wardName;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCaste() {
		return caste;
	}
	public void setCaste(String caste) {
		this.caste = caste;
	}
	public String getCastCertIssuingAuthority() {
		return castCertIssuingAuthority;
	}
	public void setCastCertIssuingAuthority(String castCertIssuingAuthority) {
		this.castCertIssuingAuthority = castCertIssuingAuthority;
	}
	public String getSerialNo() {
		return serialNo;
	}
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}
	public String getDateOfValidation() {
		return dateOfValidation;
	}
	public void setDateOfValidation(String dateOfValidation) {
		this.dateOfValidation = dateOfValidation;
	}
	public String getReceiptNo() {
		return receiptNo;
	}
	public void setReceiptNo(String receiptNo) {
		this.receiptNo = receiptNo;
	}
	public String getIsMarried() {
		return isMarried;
	}
	public void setIsMarried(String isMarried) {
		this.isMarried = isMarried;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public int getIsAprrove() {
		return isAprrove;
	}
	public void setIsAprrove(int isAprrove) {
		this.isAprrove = isAprrove;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getDateOfCertificateIssue() {
		return dateOfCertificateIssue;
	}
	public void setDateOfCertificateIssue(String dateOfCertificateIssue) {
		this.dateOfCertificateIssue = dateOfCertificateIssue;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
      
}
