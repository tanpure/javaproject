package com.nomination.model;

public class CasteMaster {

	private int id;
	private String caste_name;
	private String caste_nameLL;
	private String CasteType;
	private String category;
	private String cbc_code;
	private String Status;
	private String electionProgramName;
    private String localBodyId;
    
	public String getElectionProgramName() {
		return electionProgramName;
	}
	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}
	public String getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCaste_name() {
		return caste_name;
	}
	public void setCaste_name(String caste_name) {
		this.caste_name = caste_name;
	}
	public String getCaste_nameLL() {
		return caste_nameLL;
	}
	public void setCaste_nameLL(String caste_nameLL) {
		this.caste_nameLL = caste_nameLL;
	}
	public String getCasteType() {
		return CasteType;
	}
	public void setCasteType(String casteType) {
		CasteType = casteType;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCbc_code() {
		return cbc_code;
	}
	public void setCbc_code(String cbc_code) {
		this.cbc_code = cbc_code;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	
	
}
