package com.nomination.model;

public class CheckedDocuments {
	
	private int id;
	
	private String nominationId;
	private String documentName;
	private int doc1;
	private int doc2;
	private int doc3;
	private int doc4;
	private int doc5;
	private int doc6;
	private int doc7;
	private int doc8;
	private int doc9;
	private int doc10;
	private int doc11;
	private int doc12;
	private int doc13;
	private int doc14;
	private int doc15;
	private String electionProgramName;
    private String localBodyId;
    
	public String getElectionProgramName() {
		return electionProgramName;
	}
	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}
	public String getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getNominationId() {
		return nominationId;
	}
	public void setNominationId(String nominationId) {
		this.nominationId = nominationId;
	}
	public int getDoc1() {
		return doc1;
	}
	public void setDoc1(int doc1) {
		this.doc1 = doc1;
	}
	public int getDoc2() {
		return doc2;
	}
	public void setDoc2(int doc2) {
		this.doc2 = doc2;
	}
	public int getDoc3() {
		return doc3;
	}
	public void setDoc3(int doc3) {
		this.doc3 = doc3;
	}
	public int getDoc4() {
		return doc4;
	}
	public void setDoc4(int doc4) {
		this.doc4 = doc4;
	}
	public int getDoc5() {
		return doc5;
	}
	public void setDoc5(int doc5) {
		this.doc5 = doc5;
	}
	public int getDoc6() {
		return doc6;
	}
	public void setDoc6(int doc6) {
		this.doc6 = doc6;
	}
	public int getDoc7() {
		return doc7;
	}
	public void setDoc7(int doc7) {
		this.doc7 = doc7;
	}
	public int getDoc8() {
		return doc8;
	}
	public void setDoc8(int doc8) {
		this.doc8 = doc8;
	}
	public int getDoc9() {
		return doc9;
	}
	public void setDoc9(int doc9) {
		this.doc9 = doc9;
	}
	public int getDoc10() {
		return doc10;
	}
	public void setDoc10(int doc10) {
		this.doc10 = doc10;
	}
	public int getDoc11() {
		return doc11;
	}
	public void setDoc11(int doc11) {
		this.doc11 = doc11;
	}
	public int getDoc12() {
		return doc12;
	}
	public void setDoc12(int doc12) {
		this.doc12 = doc12;
	}
	public int getDoc13() {
		return doc13;
	}
	public void setDoc13(int doc13) {
		this.doc13 = doc13;
	}
	public int getDoc14() {
		return doc14;
	}
	public void setDoc14(int doc14) {
		this.doc14 = doc14;
	}
	public int getDoc15() {
		return doc15;
	}
	public void setDoc15(int doc15) {
		this.doc15 = doc15;
	}
	
	
}
