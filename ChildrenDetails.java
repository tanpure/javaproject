package com.nomination.model;

import java.io.Serializable;

public class ChildrenDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int affChildId;
	private int totalNoofChildren;
	private int totalNoofChildrenAfter;
	private String birthDate;
	private int childCount;
	private String uId;
	private int affId;
	private int isSubmitted;
	private int isApprove;
	private String createdBy;
	private String modifiedBy;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public int getAffChildId() {
		return affChildId;
	}

	public void setAffChildId(int affChildId) {
		this.affChildId = affChildId;
	}

	public int getTotalNoofChildren() {
		return totalNoofChildren;
	}

	public void setTotalNoofChildren(int totalNoofChildren) {
		this.totalNoofChildren = totalNoofChildren;
	}

	public int getTotalNoofChildrenAfter() {
		return totalNoofChildrenAfter;
	}

	public void setTotalNoofChildrenAfter(int totalNoofChildrenAfter) {
		this.totalNoofChildrenAfter = totalNoofChildrenAfter;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public String getuId() {
		return uId;
	}

	public void setuId(String uId) {
		this.uId = uId;
	}

	public int getAffId() {
		return affId;
	}

	public void setAffId(int affId) {
		this.affId = affId;
	}

	public int getIsSubmitted() {
		return isSubmitted;
	}

	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}

	public int getIsApprove() {
		return isApprove;
	}

	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public int getIsStatus() {
		return isStatus;
	}

	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
