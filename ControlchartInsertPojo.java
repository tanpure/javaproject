package com.nomination.model;

public class ControlchartInsertPojo {
private int id;
private int acNo;
private double latitude;
private	double longitude;
private	int partNo;
private	int srNoFrom;
private	int srNoTo;
private int voterStatus;
private String mobileNo;
private	String toUser;
private String fromUser;
private	int wardNo;
private int roleType;
private	int mobileId;
private	int sectionNo;
private int localTableId;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getAcNo() {
	return acNo;
}
public void setAcNo(int acNo) {
	this.acNo = acNo;
}
public double getLatitude() {
	return latitude;
}
public void setLatitude(double latitude) {
	this.latitude = latitude;
}
public double getLongitude() {
	return longitude;
}
public void setLongitude(double longitude) {
	this.longitude = longitude;
}
public int getPartNo() {
	return partNo;
}
public void setPartNo(int partNo) {
	this.partNo = partNo;
}
public int getSrNoFrom() {
	return srNoFrom;
}
public void setSrNoFrom(int srNoFrom) {
	this.srNoFrom = srNoFrom;
}
public int getSrNoTo() {
	return srNoTo;
}
public void setSrNoTo(int srNoTo) {
	this.srNoTo = srNoTo;
}
public int getVoterStatus() {
	return voterStatus;
}
public void setVoterStatus(int voterStatus) {
	this.voterStatus = voterStatus;
}
public String getMobileNo() {
	return mobileNo;
}
public void setMobileNo(String mobileNo) {
	this.mobileNo = mobileNo;
}
public String getToUser() {
	return toUser;
}
public void setToUser(String toUser) {
	this.toUser = toUser;
}
public String getFromUser() {
	return fromUser;
}
public void setFromUser(String fromUser) {
	this.fromUser = fromUser;
}
public int getWardNo() {
	return wardNo;
}
public void setWardNo(int wardNo) {
	this.wardNo = wardNo;
}
public int getRoleType() {
	return roleType;
}
public void setRoleType(int roleType) {
	this.roleType = roleType;
}
public int getMobileId() {
	return mobileId;
}
public void setMobileId(int mobileId) {
	this.mobileId = mobileId;
}
public int getSectionNo() {
	return sectionNo;
}
public void setSectionNo(int sectionNo) {
	this.sectionNo = sectionNo;
}
public int getLocalTableId() {
	return localTableId;
}
public void setLocalTableId(int localTableId) {
	this.localTableId = localTableId;
}


}
