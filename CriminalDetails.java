package com.nomination.model;

import java.io.Serializable;

public class CriminalDetails implements Serializable {
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int affCognizanceId;
	private int affConvictedId;
	private int affId;
	private String uId;
	private String nameAct;
	private String sectionsAct;
	private String nameCourt;
	private String caseNumber;
	private String dateCognizance;
	private String sectionsAct1;
	private String sectionsAct2;
	private String sectionsAct3;
	private String sectionsAct4;
	private String sectionsAct5;
	private String sectionsAct6;
	private int noCriminal;
	private int rdbAppeal;
	private String nameAppelateCourt;
	private String dateFillingAppeal;
	private int appealNumber;
	private String statusAppealConvictedid;
	private int isSubmitted;
	private int isApprove;
	private String createdBy;
	private String modifiedBy;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getAffCognizanceId() {
		return affCognizanceId;
	}
	public void setAffCognizanceId(int affCognizanceId) {
		this.affCognizanceId = affCognizanceId;
	}
	public int getAffId() {
		return affId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getNameAct() {
		return nameAct;
	}
	public void setNameAct(String nameAct) {
		this.nameAct = nameAct;
	}
	public String getSectionsAct() {
		return sectionsAct;
	}
	public void setSectionsAct(String sectionsAct) {
		this.sectionsAct = sectionsAct;
	}
	public String getNameCourt() {
		return nameCourt;
	}
	public void setNameCourt(String nameCourt) {
		this.nameCourt = nameCourt;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getDateCognizance() {
		return dateCognizance;
	}
	public void setDateCognizance(String dateCognizance) {
		this.dateCognizance = dateCognizance;
	}
	public String getSectionsAct1() {
		return sectionsAct1;
	}
	public void setSectionsAct1(String sectionsAct1) {
		this.sectionsAct1 = sectionsAct1;
	}
	public String getSectionsAct2() {
		return sectionsAct2;
	}
	public void setSectionsAct2(String sectionsAct2) {
		this.sectionsAct2 = sectionsAct2;
	}
	public String getSectionsAct3() {
		return sectionsAct3;
	}
	public void setSectionsAct3(String sectionsAct3) {
		this.sectionsAct3 = sectionsAct3;
	}
	public String getSectionsAct4() {
		return sectionsAct4;
	}
	public void setSectionsAct4(String sectionsAct4) {
		this.sectionsAct4 = sectionsAct4;
	}
	public String getSectionsAct5() {
		return sectionsAct5;
	}
	public void setSectionsAct5(String sectionsAct5) {
		this.sectionsAct5 = sectionsAct5;
	}
	public String getSectionsAct6() {
		return sectionsAct6;
	}
	public void setSectionsAct6(String sectionsAct6) {
		this.sectionsAct6 = sectionsAct6;
	}
	public int getNoCriminal() {
		return noCriminal;
	}
	public void setNoCriminal(int noCriminal) {
		this.noCriminal = noCriminal;
	}
	
	public int getAffConvictedId() {
		return affConvictedId;
	}
	public int getRdbAppeal() {
		return rdbAppeal;
	}
	public String getNameAppelateCourt() {
		return nameAppelateCourt;
	}
	public String getDateFillingAppeal() {
		return dateFillingAppeal;
	}
	public int getAppealNumber() {
		return appealNumber;
	}
	public String getStatusAppealConvictedid() {
		return statusAppealConvictedid;
	}
	public void setAffConvictedId(int affConvictedId) {
		this.affConvictedId = affConvictedId;
	}
	public void setRdbAppeal(int rdbAppeal) {
		this.rdbAppeal = rdbAppeal;
	}
	public void setNameAppelateCourt(String nameAppelateCourt) {
		this.nameAppelateCourt = nameAppelateCourt;
	}
	public void setDateFillingAppeal(String dateFillingAppeal) {
		this.dateFillingAppeal = dateFillingAppeal;
	}
	public void setAppealNumber(int appealNumber) {
		this.appealNumber = appealNumber;
	}
	public void setStatusAppealConvictedid(String statusAppealConvictedid) {
		this.statusAppealConvictedid = statusAppealConvictedid;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
