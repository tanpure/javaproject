package com.nomination.model;

public class DailyExpensePojo {
	private int id;
	private String date;
	private int expenseId;
	private String expenseType;
	private int subExpenseId;
	private String subExpenseType;
	private String qty_Size_Area;
	private double rate;
	private double totalExpense;
	private String paymentMode;
	private String chequeNo;
	private double paidAmount;
	private String invoiceNo;
	private String firmName;
	private String firmOwnerMobNo;
	private String imeiNumber;
	private String unit;
	private String paymentType;
	private String standardRate;
	private double balancePayment;
	private String sourceOfExpense;
	private String partyName;
	private String partyNo;
	//private int distId;
	//private int localBodyType;
	//private int localBodyId;
	private int wardNo;
	private int candidateRole;
	private String reffrenceMobile;
	private String deviation;
	private String officerMoNo;
	private String offAcceptStatus;
	private String statusUpdateDate;
	private String addExpOfficerRole;
	private int groupExpId;
	private int refExpenseOldId;
	private int diffAmount;
	private String decisionRemark;
	private int acceptedEntry;
	private String deviationMsg;
	private String deviationOnVisitedDate;
	private int isApproved;
	private String entryFrom;
	private int isActive;
	private int isPrinted;
	//private String createdBy;
	private String modifiedBy;
//	private int epId;
	private int stateId;
	private String uId;
	private String token;
	private String firstName;
	private String middleName;
	private String lastName;
	private String userMobileNo;
	private String printFor;
	private String nominationId;
	private int seatId;
	private String expForCand;
	private int localTableId;
	private int totcnt;
	private double totsum;
	private String delaycnt;
	private int deviationCount;

	private int epId;
	private int distId;
	private int localBodyId;
	private int localBodyType;
	//private String expenseType;
	//private String subExpenseType;
	private String localBodyName;
	private String createdBy;
	private String candidateId;
	private String forCandidate;
	private String noticeDate;
	private String remarkByRo;
	private String printedDate;
	private String createdDate;
	private String modifiedDate;
	
	
	
	
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getPrintedDate() {
		return printedDate;
	}
	public void setPrintedDate(String printedDate) {
		this.printedDate = printedDate;
	}
	public String getRemarkByRo() {
		return remarkByRo;
	}
	public void setRemarkByRo(String remarkByRo) {
		this.remarkByRo = remarkByRo;
	}
	public String getNoticeDate() {
		return noticeDate;
	}
	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}
	public String getForCandidate() {
		return forCandidate;
	}
	public void setForCandidate(String forCandidate) {
		this.forCandidate = forCandidate;
	}
	public String getCandidateId() {
		return candidateId;
	}
	public void setCandidateId(String candidateId) {
		this.candidateId = candidateId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(int expenseId) {
		this.expenseId = expenseId;
	}
	public int getSubExpenseId() {
		return subExpenseId;
	}
	public void setSubExpenseId(int subExpenseId) {
		this.subExpenseId = subExpenseId;
	}
	public String getQty_Size_Area() {
		return qty_Size_Area;
	}
	public void setQty_Size_Area(String qty_Size_Area) {
		this.qty_Size_Area = qty_Size_Area;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getTotalExpense() {
		return totalExpense;
	}
	public void setTotalExpense(double totalExpense) {
		this.totalExpense = totalExpense;
	}
	public String getPaymentMode() {
		return paymentMode;
	}
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	public String getChequeNo() {
		return chequeNo;
	}
	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}
	public double getPaidAmount() {
		return paidAmount;
	}
	public void setPaidAmount(double paidAmount) {
		this.paidAmount = paidAmount;
	}
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getFirmName() {
		return firmName;
	}
	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}
	public String getFirmOwnerMobNo() {
		return firmOwnerMobNo;
	}
	public void setFirmOwnerMobNo(String firmOwnerMobNo) {
		this.firmOwnerMobNo = firmOwnerMobNo;
	}
	public String getImeiNumber() {
		return imeiNumber;
	}
	public void setImeiNumber(String imeiNumber) {
		this.imeiNumber = imeiNumber;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	public String getStandardRate() {
		return standardRate;
	}
	public void setStandardRate(String standardRate) {
		this.standardRate = standardRate;
	}
	public double getBalancePayment() {
		return balancePayment;
	}
	public void setBalancePayment(double balancePayment) {
		this.balancePayment = balancePayment;
	}
	public String getSourceOfExpense() {
		return sourceOfExpense;
	}
	public void setSourceOfExpense(String sourceOfExpense) {
		this.sourceOfExpense = sourceOfExpense;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getPartyNo() {
		return partyNo;
	}
	public void setPartyNo(String partyNo) {
		this.partyNo = partyNo;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public int getCandidateRole() {
		return candidateRole;
	}
	public void setCandidateRole(int candidateRole) {
		this.candidateRole = candidateRole;
	}
	public String getReffrenceMobile() {
		return reffrenceMobile;
	}
	public void setReffrenceMobile(String reffrenceMobile) {
		this.reffrenceMobile = reffrenceMobile;
	}
	public String getDeviation() {
		return deviation;
	}
	public void setDeviation(String deviation) {
		this.deviation = deviation;
	}
	public String getOfficerMoNo() {
		return officerMoNo;
	}
	public void setOfficerMoNo(String officerMoNo) {
		this.officerMoNo = officerMoNo;
	}
	public String getOffAcceptStatus() {
		return offAcceptStatus;
	}
	public void setOffAcceptStatus(String offAcceptStatus) {
		this.offAcceptStatus = offAcceptStatus;
	}
	public String getStatusUpdateDate() {
		return statusUpdateDate;
	}
	public void setStatusUpdateDate(String statusUpdateDate) {
		this.statusUpdateDate = statusUpdateDate;
	}
	public String getAddExpOfficerRole() {
		return addExpOfficerRole;
	}
	public void setAddExpOfficerRole(String addExpOfficerRole) {
		this.addExpOfficerRole = addExpOfficerRole;
	}
	public int getGroupExpId() {
		return groupExpId;
	}
	public void setGroupExpId(int groupExpId) {
		this.groupExpId = groupExpId;
	}
	public int getRefExpenseOldId() {
		return refExpenseOldId;
	}
	public void setRefExpenseOldId(int refExpenseOldId) {
		this.refExpenseOldId = refExpenseOldId;
	}
	public int getDiffAmount() {
		return diffAmount;
	}
	public void setDiffAmount(int diffAmount) {
		this.diffAmount = diffAmount;
	}
	public String getDecisionRemark() {
		return decisionRemark;
	}
	public void setDecisionRemark(String decisionRemark) {
		this.decisionRemark = decisionRemark;
	}
	public int getAcceptedEntry() {
		return acceptedEntry;
	}
	public void setAcceptedEntry(int acceptedEntry) {
		this.acceptedEntry = acceptedEntry;
	}
	public String getDeviationMsg() {
		return deviationMsg;
	}
	public void setDeviationMsg(String deviationMsg) {
		this.deviationMsg = deviationMsg;
	}
	public String getDeviationOnVisitedDate() {
		return deviationOnVisitedDate;
	}
	public void setDeviationOnVisitedDate(String deviationOnVisitedDate) {
		this.deviationOnVisitedDate = deviationOnVisitedDate;
	}
	public int getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}
	public String getEntryFrom() {
		return entryFrom;
	}
	public void setEntryFrom(String entryFrom) {
		this.entryFrom = entryFrom;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public int getIsPrinted() {
		return isPrinted;
	}
	public void setIsPrinted(int isPrinted) {
		this.isPrinted = isPrinted;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getStateId() {
		return stateId;
	}
	public void setStateId(int stateId) {
		this.stateId = stateId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserMobileNo() {
		return userMobileNo;
	}
	public void setUserMobileNo(String userMobileNo) {
		this.userMobileNo = userMobileNo;
	}
	public String getPrintFor() {
		return printFor;
	}
	public void setPrintFor(String printFor) {
		this.printFor = printFor;
	}
	public String getNominationId() {
		return nominationId;
	}
	public void setNominationId(String nominationId) {
		this.nominationId = nominationId;
	}
	public int getSeatId() {
		return seatId;
	}
	public void setSeatId(int seatId) {
		this.seatId = seatId;
	}
	public String getExpForCand() {
		return expForCand;
	}
	public void setExpForCand(String expForCand) {
		this.expForCand = expForCand;
	}
	public int getLocalTableId() {
		return localTableId;
	}
	public void setLocalTableId(int localTableId) {
		this.localTableId = localTableId;
	}
	public int getTotcnt() {
		return totcnt;
	}
	public void setTotcnt(int totcnt) {
		this.totcnt = totcnt;
	}
	public double getTotsum() {
		return totsum;
	}
	public void setTotsum(double totsum) {
		this.totsum = totsum;
	}
	public String getDelaycnt() {
		return delaycnt;
	}
	public void setDelaycnt(String delaycnt) {
		this.delaycnt = delaycnt;
	}
	public int getDeviationCount() {
		return deviationCount;
	}
	public void setDeviationCount(int deviationCount) {
		this.deviationCount = deviationCount;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getDistId() {
		return distId;
	}
	public void setDistId(int distId) {
		this.distId = distId;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getLocalBodyType() {
		return localBodyType;
	}
	public void setLocalBodyType(int localBodyType) {
		this.localBodyType = localBodyType;
	}
	public String getExpenseType() {
		return expenseType;
	}
	public void setExpenseType(String expenseType) {
		this.expenseType = expenseType;
	}
	public String getSubExpenseType() {
		return subExpenseType;
	}
	public void setSubExpenseType(String subExpenseType) {
		this.subExpenseType = subExpenseType;
	}
	public String getLocalBodyName() {
		return localBodyName;
	}
	public void setLocalBodyName(String localBodyName) {
		this.localBodyName = localBodyName;
	}

	
	
}
