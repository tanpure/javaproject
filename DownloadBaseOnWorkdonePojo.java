package com.nomination.model;

public class DownloadBaseOnWorkdonePojo {
	private int id;
	private int acNo;
	private int partNo;
	private int epId;
	private int sectionCount;
	private String voterCount;
	private int newSectionCount;
	private int detailsUpdated;
	private String createdBy;
	private String  modifiedBy;
	private String createdDate;
	private String modifiedDate;
	private float workdone;
	private String juniorName;
	private String juniorRole;
	private String mobile_no;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAcNo() {
		return acNo;
	}
	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}
	public int getPartNo() {
		return partNo;
	}
	public void setPartNo(int partNo) {
		this.partNo = partNo;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getSectionCount() {
		return sectionCount;
	}
	public void setSectionCount(int sectionCount) {
		this.sectionCount = sectionCount;
	}
	public String getVoterCount() {
		return voterCount;
	}
	public void setVoterCount(String voterCount) {
		this.voterCount = voterCount;
	}
	public int getNewSectionCount() {
		return newSectionCount;
	}
	public void setNewSectionCount(int newSectionCount) {
		this.newSectionCount = newSectionCount;
	}
	public int getDetailsUpdated() {
		return detailsUpdated;
	}
	public void setDetailsUpdated(int detailsUpdated) {
		this.detailsUpdated = detailsUpdated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public float getWorkdone() {
		return workdone;
	}
	public void setWorkdone(float workdone) {
		this.workdone = workdone;
	}
	public String getJuniorName() {
		return juniorName;
	}
	public void setJuniorName(String juniorName) {
		this.juniorName = juniorName;
	}
	public String getJuniorRole() {
		return juniorRole;
	}
	public void setJuniorRole(String juniorRole) {
		this.juniorRole = juniorRole;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}

	
}
