package com.nomination.model;

import java.io.Serializable;

public class EducationDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int affEducationId;
	private int affId;
	private String nameCollege;
	private String nameUniversity;
	private String qualification;
	private String yearPassing;
	private String percentage;
	/*private String sworn;
	private String fullNamePerson;
	private String surnameNamePer;
	private String firstNamePer;
	private String fatherNamePer;
	private String dateAffirmation;
	private String valueStamp;*/
	private String uId;
	private String createdBy;
	private String modifiedBy;
	private int isSubmitted;
	private int isApprove;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getAffEducationId() {
		return affEducationId;
	}
	public int getAffId() {
		return affId;
	}
	public String getNameCollege() {
		return nameCollege;
	}
	public String getNameUniversity() {
		return nameUniversity;
	}
	public String getQualification() {
		return qualification;
	}
	public String getYearPassing() {
		return yearPassing;
	}
	public String getPercentage() {
		return percentage;
	}
	/*public String getSworn() {
		return sworn;
	}
	public String getFullNamePerson() {
		return fullNamePerson;
	}
	public String getSurnameNamePer() {
		return surnameNamePer;
	}
	public String getFirstNamePer() {
		return firstNamePer;
	}
	public String getFatherNamePer() {
		return fatherNamePer;
	}
	public String getDateAffirmation() {
		return dateAffirmation;
	}
	public String getValueStamp() {
		return valueStamp;
	}*/
	public String getuId() {
		return uId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setAffEducationId(int affEducationId) {
		this.affEducationId = affEducationId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public void setNameCollege(String nameCollege) {
		this.nameCollege = nameCollege;
	}
	public void setNameUniversity(String nameUniversity) {
		this.nameUniversity = nameUniversity;
	}
	public void setQualification(String qualification) {
		this.qualification = qualification;
	}
	public void setYearPassing(String yearPassing) {
		this.yearPassing = yearPassing;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	/*public void setSworn(String sworn) {
		this.sworn = sworn;
	}
	public void setFullNamePerson(String fullNamePerson) {
		this.fullNamePerson = fullNamePerson;
	}
	public void setSurnameNamePer(String surnameNamePer) {
		this.surnameNamePer = surnameNamePer;
	}
	public void setFirstNamePer(String firstNamePer) {
		this.firstNamePer = firstNamePer;
	}
	public void setFatherNamePer(String fatherNamePer) {
		this.fatherNamePer = fatherNamePer;
	}
	public void setDateAffirmation(String dateAffirmation) {
		this.dateAffirmation = dateAffirmation;
	}
	public void setValueStamp(String valueStamp) {
		this.valueStamp = valueStamp;
	}*/
	public void setuId(String uId) {
		this.uId = uId;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	

}
