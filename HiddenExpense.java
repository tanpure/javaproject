package com.nomination.model;

public class HiddenExpense {
	private int id;
	private int districtId;
	private int localBodyId;
	private String localBodyType;
	private int wardNo;
	private int amount;
	private String evidance;
	private String expenseHead;
	private String subExpenseHead;
	private String createdBy;
	private String modifyBy;
	private String candidateName;
	private String mobileNo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public String getLocalBodyType() {
		return localBodyType;
	}
	public void setLocalBodyType(String localBodyType) {
		this.localBodyType = localBodyType;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public String getEvidance() {
		return evidance;
	}
	public void setEvidance(String evidance) {
		this.evidance = evidance;
	}
	public String getExpenseHead() {
		return expenseHead;
	}
	public void setExpenseHead(String expenseHead) {
		this.expenseHead = expenseHead;
	}
	public String getSubExpenseHead() {
		return subExpenseHead;
	}
	public void setSubExpenseHead(String subExpenseHead) {
		this.subExpenseHead = subExpenseHead;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifyBy() {
		return modifyBy;
	}
	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}
	public String getCandidateName() {
		return candidateName;
	}
	public void setCandidateName(String candidateName) {
		this.candidateName = candidateName;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	////getter  and Setter
	
	
}

