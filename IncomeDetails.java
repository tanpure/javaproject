package com.nomination.model;

import java.io.Serializable;

public class IncomeDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int incomeId;
	private int candidateType;
	private int isHasPancard;
	private String panNumber;
	private int affId;
	private String incomeTaxPaidYear;
	private String incomeTaxPaidYearText;
	private double incomeAmount;
	private int spouseOccupationId;
	private String spouseOccupationName;
	private String createdBy;
	private String modifiedBy;
	private String uId;
	private int isSubmitted;
	private int isApprove;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	public int getIncomeId() {
		return incomeId;
	}
	public void setIncomeId(int incomeId) {
		this.incomeId = incomeId;
	}
	public int getCandidateType() {
		return candidateType;
	}
	public void setCandidateType(int candidateType) {
		this.candidateType = candidateType;
	}
	public int getIsHasPancard() {
		return isHasPancard;
	}
	public void setIsHasPancard(int isHasPancard) {
		this.isHasPancard = isHasPancard;
	}
	public String getPanNumber() {
		return panNumber;
	}
	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}
	public int getAffId() {
		return affId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public String getIncomeTaxPaidYear() {
		return incomeTaxPaidYear;
	}
	public void setIncomeTaxPaidYear(String incomeTaxPaidYear) {
		this.incomeTaxPaidYear = incomeTaxPaidYear;
	}
	public String getIncomeTaxPaidYearText() {
		return incomeTaxPaidYearText;
	}
	public void setIncomeTaxPaidYearText(String incomeTaxPaidYearText) {
		this.incomeTaxPaidYearText = incomeTaxPaidYearText;
	}
	public double getIncomeAmount() {
		return incomeAmount;
	}
	public void setIncomeAmount(double incomeAmount) {
		this.incomeAmount = incomeAmount;
	}
	public int getSpouseOccupationId() {
		return spouseOccupationId;
	}
	public void setSpouseOccupationId(int spouseOccupationId) {
		this.spouseOccupationId = spouseOccupationId;
	}
	public String getSpouseOccupationName() {
		return spouseOccupationName;
	}
	public void setSpouseOccupationName(String spouseOccupationName) {
		this.spouseOccupationName = spouseOccupationName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
