package com.nomination.model;

import java.io.Serializable;

public class IncomeTaxDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int incomeTaxId;
	private int affId;
	private String uId;
	private int taxType;
	private String incomeTaxAssessmentYear;
	private double taxAmount;
	private int propertyType;
	private String createdBy;
	private String modifiedBy;
	private int isSubmitted;
	private int isApprove;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getIncomeTaxId() {
		return incomeTaxId;
	}
	public int getAffId() {
		return affId;
	}
	public String getuId() {
		return uId;
	}
	public int getTaxType() {
		return taxType;
	}
	public String getIncomeTaxAssessmentYear() {
		return incomeTaxAssessmentYear;
	}
	public double getTaxAmount() {
		return taxAmount;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setIncomeTaxId(int incomeTaxId) {
		this.incomeTaxId = incomeTaxId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public void setTaxType(int taxType) {
		this.taxType = taxType;
	}
	public void setIncomeTaxAssessmentYear(String incomeTaxAssessmentYear) {
		this.incomeTaxAssessmentYear = incomeTaxAssessmentYear;
	}
	public void setTaxAmount(double taxAmount) {
		this.taxAmount = taxAmount;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(int propertyType) {
		this.propertyType = propertyType;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
}
