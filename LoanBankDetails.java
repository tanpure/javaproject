package com.nomination.model;

import java.io.Serializable;

public class LoanBankDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int lbID;
	private int fiId;
	private int giId;
	private int affId;
	private String bankName;
	private String loanAmount;
	private String giName;
	private String giLoanAmount;
	private int propertyType;
	private String createdBy;
	private String modifiedBy;
	private String uId;
	private int isSubmitted;
	private int isApprove;
	private int lfId;
	private String fiLoanAmount;
	private String fiName;
	private String typeName;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getLbID() {
		return lbID;
	}
	public int getAffId() {
		return affId;
	}
	public String getBankName() {
		return bankName;
	}
	public String getLoanAmount() {
		return loanAmount;
	}
	public int getPropertyType() {
		return propertyType;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public String getuId() {
		return uId;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setLbID(int lbID) {
		this.lbID = lbID;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}
	public void setPropertyType(int propertyType) {
		this.propertyType = propertyType;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getLfId() {
		return lfId;
	}
	public String getFiLoanAmount() {
		return fiLoanAmount;
	}
	public String getFiName() {
		return fiName;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setLfId(int lfId) {
		this.lfId = lfId;
	}
	public void setFiLoanAmount(String fiLoanAmount) {
		this.fiLoanAmount = fiLoanAmount;
	}
	public void setFiName(String fiName) {
		this.fiName = fiName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public int getFiId() {
		return fiId;
	}
	public void setFiId(int fiId) {
		this.fiId = fiId;
	}
	public int getGiId() {
		return giId;
	}
	public void setGiId(int giId) {
		this.giId = giId;
	}
	public String getGiName() {
		return giName;
	}
	public void setGiName(String giName) {
		this.giName = giName;
	}
	public String getGiLoanAmount() {
		return giLoanAmount;
	}
	public void setGiLoanAmount(String giLoanAmount) {
		this.giLoanAmount = giLoanAmount;
	}
}
