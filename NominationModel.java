package com.nomination.model;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

public class NominationModel {

		private int nominationId;
		private String nominationRegNo;
		private String uId;
		private String electionProgram;
		private String electionYear;
		private String electionID;
		private int localBodyID;
		private String religion;
		private	String distId;
		private int wardId;
		private String wardName;
		private String reservation;
		private String aadharNo;
		private String firstName;
		private String middleName;
		private String lastName;
		private String firstName_LL;
		private String middleName_LL;
		private String lastName_LL;
		private String candidateMobile;
		private String candidateEmail;
		private String gender;
		private String dob;
		private int age;
		private int occupation;
		private String address;
		private String pin;
		private String category;
		private String caste;
		private String womenReservedSeat;
		private String candidature;
		private int partyCategoryId;
		private int nameOfPartyId;
		private int symbolChoice1;
		private int symbolChoice2;
		private int symbolChoice3;
		private int candidateWardNo;
		private String candidateSerialNo;
		private String proposerFirstName1;
		private String proposerLastName1;
		private String proposerForHName1;
		private String proposerWardNo1;
		private String proposerSerialNo1;
		private String proposerVoterListPartNo1;
		private String proposerFirstName2;
		private String proposerLastName2;
		private String proposerForHName2;
		private String proposerWardNo2;
		private String proposerSerialNo2;
		private String proposerVoterListPartNo2;
		
		private String proposerFirstName3;
		private String proposerLastName3;
		private String proposerForHName3;
		private String proposerWardNo3;
		private String proposerSerialNo3;
		private String proposerVoterListPartNo3;
		
		private String proposerFirstName4;
		private String proposerLastName4;
		private String proposerForHName4;
		private String proposerWardNo4;
		private String proposerSerialNo4;
		private String proposerVoterListPartNo4;
		
		private String proposerFirstName5;
		private String proposerLastName5;
		private String proposerForHName5;
		private String proposerWardNo5;
		private String proposerSerialNo5;
		private String proposerVoterListPartNo5;
		
		private String willingness;
		private String rdb_AgeValidation;
		private String rdb_EligibleElection;
		private String rdb_WhetherContractor;
		private String rdb_OutStanding;
		private String reservedCategory;
		private String createdBy;
		private String electionProgName;
		private String localBodyName;
		private int reportType;
		private int seatId;
		private String mobileNo;
		private String emailId;
		private String reg_Type;
		private int mainCaste;
		private int langId;
		private String su_dateTime;
		private int su_status;
		private String su_reason;
		private String duplicate_ro_ID;
		private String withdrawalDate;
		private int isActive;
		private int status;
		private int groupID;
		private String formType;
		private int municipalCorporationId;
		private int nagarPanchayatId;
		private String candidateVoterListPartNo;
		private String partyNo;
		private int aff_FinalSumbission;
		private String aff_FinalSumbissionOn;
		private String aff_FinalSumbissionBy;
		private String aff_FinalSumbissionCk;
		private String aff_FinalSumbissionIP;
		private int indChoice1Id;
		private int indChoice2Id;
		private int indChoice3Id;
		private int statusPending;
		private int statusAccepted;
		private int statusRejected;
		
		
		
	
		
		public int getStatusPending() {
			return statusPending;
		}

		public void setStatusPending(int statusPending) {
			this.statusPending = statusPending;
		}

		public int getStatusAccepted() {
			return statusAccepted;
		}

		public void setStatusAccepted(int statusAccepted) {
			this.statusAccepted = statusAccepted;
		}

		public int getStatusRejected() {
			return statusRejected;
		}

		public void setStatusRejected(int statusRejected) {
			this.statusRejected = statusRejected;
		}

		public int getIndChoice1Id() {
			return indChoice1Id;
		}

		public void setIndChoice1Id(int indChoice1Id) {
			this.indChoice1Id = indChoice1Id;
		}

		public int getIndChoice2Id() {
			return indChoice2Id;
		}

		public void setIndChoice2Id(int indChoice2Id) {
			this.indChoice2Id = indChoice2Id;
		}

		public int getIndChoice3Id() {
			return indChoice3Id;
		}

		public void setIndChoice3Id(int indChoice3Id) {
			this.indChoice3Id = indChoice3Id;
		}

		public String getMobileNo() {
			return mobileNo;
		}

		public void setMobileNo(String mobileNo) {
			this.mobileNo = mobileNo;
		}

		public String getEmailId() {
			return emailId;
		}

		public void setEmailId(String emailId) {
			this.emailId = emailId;
		}

		public String getReg_Type() {
			return reg_Type;
		}

		public void setReg_Type(String reg_Type) {
			this.reg_Type = reg_Type;
		}

		public int getMainCaste() {
			return mainCaste;
		}

		public void setMainCaste(int mainCaste) {
			this.mainCaste = mainCaste;
		}

		public int getLangId() {
			return langId;
		}

		public void setLangId(int langId) {
			this.langId = langId;
		}

		public String getSu_dateTime() {
			return su_dateTime;
		}

		public void setSu_dateTime(String su_dateTime) {
			this.su_dateTime = su_dateTime;
		}

		public String getSu_reason() {
			return su_reason;
		}
		
		public int getSu_status() {
			return su_status;
		}

		public void setSu_status(int su_status) {
			this.su_status = su_status;
		}

		public void setSu_reason(String su_reason) {
			this.su_reason = su_reason;
		}

		public String getDuplicate_ro_ID() {
			return duplicate_ro_ID;
		}

		public void setDuplicate_ro_ID(String duplicate_ro_ID) {
			this.duplicate_ro_ID = duplicate_ro_ID;
		}

		public String getWithdrawalDate() {
			return withdrawalDate;
		}

		public void setWithdrawalDate(String withdrawalDate) {
			this.withdrawalDate = withdrawalDate;
		}

		public int getIsActive() {
			return isActive;
		}

		public void setIsActive(int isActive) {
			this.isActive = isActive;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public int getGroupID() {
			return groupID;
		}

		public void setGroupID(int groupID) {
			this.groupID = groupID;
		}

		public String getFormType() {
			return formType;
		}

		public void setFormType(String formType) {
			this.formType = formType;
		}

		public int getMunicipalCorporationId() {
			return municipalCorporationId;
		}

		public void setMunicipalCorporationId(int municipalCorporationId) {
			this.municipalCorporationId = municipalCorporationId;
		}

		public int getNagarPanchayatId() {
			return nagarPanchayatId;
		}

		public void setNagarPanchayatId(int nagarPanchayatId) {
			this.nagarPanchayatId = nagarPanchayatId;
		}

		public String getCandidateVoterListPartNo() {
			return candidateVoterListPartNo;
		}

		public void setCandidateVoterListPartNo(String candidateVoterListPartNo) {
			this.candidateVoterListPartNo = candidateVoterListPartNo;
		}

		public String getPartyNo() {
			return partyNo;
		}

		public void setPartyNo(String partyNo) {
			this.partyNo = partyNo;
		}

		public int getAff_FinalSumbission() {
			return aff_FinalSumbission;
		}

		public void setAff_FinalSumbission(int aff_FinalSumbission) {
			this.aff_FinalSumbission = aff_FinalSumbission;
		}

		public String getAff_FinalSumbissionOn() {
			return aff_FinalSumbissionOn;
		}

		public void setAff_FinalSumbissionOn(String aff_FinalSumbissionOn) {
			this.aff_FinalSumbissionOn = aff_FinalSumbissionOn;
		}

		public String getAff_FinalSumbissionBy() {
			return aff_FinalSumbissionBy;
		}

		public void setAff_FinalSumbissionBy(String aff_FinalSumbissionBy) {
			this.aff_FinalSumbissionBy = aff_FinalSumbissionBy;
		}

		public String getAff_FinalSumbissionCk() {
			return aff_FinalSumbissionCk;
		}

		public void setAff_FinalSumbissionCk(String aff_FinalSumbissionCk) {
			this.aff_FinalSumbissionCk = aff_FinalSumbissionCk;
		}

		public String getAff_FinalSumbissionIP() {
			return aff_FinalSumbissionIP;
		}

		public void setAff_FinalSumbissionIP(String aff_FinalSumbissionIP) {
			this.aff_FinalSumbissionIP = aff_FinalSumbissionIP;
		}

		public String getRdb_OutStanding() {
			return rdb_OutStanding;
		}

		@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
		private String subDate;
		
		private int localBodyId;
		private String districtName;
		private String token;
		private String religionName;
		
		private String partyCategoryName;
		private String partyName;
		
		private String su_Status;
		private String su_ROID;
		private String receipt;
		private String subChk;
		
		private String withdrawal_status;
		
		private String withdrawal_roID;
		
		private String isSymbolAllotted;
		private String duplicateCandidate;
		private String isProforoma_A_B_Submitted;
		private int symbolsID;
		
		private String symbolChoiceName1;
		private String symbolChoiceName2;
		private String symbolChoiceName3;
		private String servertime;
		private String nomEndTime;
		
		
		
		public String getServertime() {
			return servertime;
		}

		public void setServertime(String servertime) {
			this.servertime = servertime;
		}

		public String getNomEndTime() {
			return nomEndTime;
		}

		public void setNomEndTime(String nomEndTime) {
			this.nomEndTime = nomEndTime;
		}

		public String getIsSymbolAllotted() {
			return isSymbolAllotted;
		}

		public void setIsSymbolAllotted(String isSymbolAllotted) {
			this.isSymbolAllotted = isSymbolAllotted;
		}

		public String getDuplicateCandidate() {
			return duplicateCandidate;
		}

		public void setDuplicateCandidate(String duplicateCandidate) {
			this.duplicateCandidate = duplicateCandidate;
		}

		public String getIsProforoma_A_B_Submitted() {
			return isProforoma_A_B_Submitted;
		}

		public void setIsProforoma_A_B_Submitted(String isProforoma_A_B_Submitted) {
			this.isProforoma_A_B_Submitted = isProforoma_A_B_Submitted;
		}

		public int getSymbolsID() {
			return symbolsID;
		}

		public void setSymbolsID(int symbolsID) {
			this.symbolsID = symbolsID;
		}

		public String getSymbolChoiceName1() {
			return symbolChoiceName1;
		}

		public void setSymbolChoiceName1(String symbolChoiceName1) {
			this.symbolChoiceName1 = symbolChoiceName1;
		}

		public String getSymbolChoiceName2() {
			return symbolChoiceName2;
		}

		public void setSymbolChoiceName2(String symbolChoiceName2) {
			this.symbolChoiceName2 = symbolChoiceName2;
		}

		public String getSymbolChoiceName3() {
			return symbolChoiceName3;
		}

		public void setSymbolChoiceName3(String symbolChoiceName3) {
			this.symbolChoiceName3 = symbolChoiceName3;
		}

		public String getWithdrawal_roID() {
			return withdrawal_roID;
		}

		public void setWithdrawal_roID(String withdrawal_roID) {
			this.withdrawal_roID = withdrawal_roID;
		}

		public String getWithdrawal_status() {
			return withdrawal_status;
		}

		public void setWithdrawal_status(String withdrawal_status) {
			this.withdrawal_status = withdrawal_status;
		}

		public String getSu_Status() {
			return su_Status;
		}

		public void setSu_Status(String su_Status) {
			this.su_Status = su_Status;
		}

		public String getSu_ROID() {
			return su_ROID;
		}

		public void setSu_ROID(String su_ROID) {
			this.su_ROID = su_ROID;
		}

		public String getReceipt() {
			return receipt;
		}

		public void setReceipt(String receipt) {
			this.receipt = receipt;
		}

		public String getSubChk() {
			return subChk;
		}

		public void setSubChk(String subChk) {
			this.subChk = subChk;
		}

		public String getPartyCategoryName() {
			return partyCategoryName;
		}

		public void setPartyCategoryName(String partyCategoryName) {
			this.partyCategoryName = partyCategoryName;
		}

		public String getPartyName() {
			return partyName;
		}

		public void setPartyName(String partyName) {
			this.partyName = partyName;
		}

		public String getReligionName() {
			return religionName;
		}

		public void setReligionName(String religionName) {
			this.religionName = religionName;
		}

		public String getToken() {
			return token;
		}

		public void setToken(String token) {
			this.token = token;
		}

		public int getLocalBodyId() {
			return localBodyId;
		}

		public void setLocalBodyId(int localBodyId) {
			this.localBodyId = localBodyId;
		}

		public String getDistrictName() {
			return districtName;
		}

		public void setDistrictName(String districtName) {
			this.districtName = districtName;
		}

		public String getSubDate() {
			return subDate;
		}

		public void setSubDate(String subDate) {
			this.subDate = subDate;
		}

		@Transient
		@JsonFormat(shape=JsonFormat.Shape.ANY, pattern="yyyy-MM-dd HH:mm:ss")
		private String createdDate;

		

		public int getNominationId() {
			return nominationId;
		}

		public void setNominationId(int nominationId) {
			this.nominationId = nominationId;
		}

		public String getNominationRegNo() {
			return nominationRegNo;
		}

		public void setNominationRegNo(String nominationRegNo) {
			this.nominationRegNo = nominationRegNo;
		}

		public String getuId() {
			return uId;
		}

		public void setuId(String uId) {
			this.uId = uId;
		}

		
		
		public String getElectionProgram() {
			return electionProgram;
		}

		public void setElectionProgram(String electionProgram) {
			this.electionProgram = electionProgram;
		}

		public String getElectionYear() {
			return electionYear;
		}

		public void setElectionYear(String electionYear) {
			this.electionYear = electionYear;
		}

		

		public String getElectionID() {
			return electionID;
		}

		public void setElectionID(String electionID) {
			this.electionID = electionID;
		}

		public int getLocalBodyID() {
			return localBodyID;
		}

		public void setLocalBodyID(int localBodyID) {
			this.localBodyID = localBodyID;
		}

		public String getReligion() {
			return religion;
		}

		public void setReligion(String religion) {
			this.religion = religion;
		}

		public String getDistId() {
			return distId;
		}

		public void setDistId(String distId) {
			this.distId = distId;
		}

		public String getReservation() {
			return reservation;
		}

		public void setReservation(String reservation) {
			this.reservation = reservation;
		}

		public int getWardId() {
			return wardId;
		}

		public void setWardId(int wardId) {
			this.wardId = wardId;
		}

		public String getWardName() {
			return wardName;
		}

		public void setWardName(String wardName) {
			this.wardName = wardName;
		}

		public String getAadharNo() {
			return aadharNo;
		}

		public void setAadharNo(String aadharNo) {
			this.aadharNo = aadharNo;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getMiddleName() {
			return middleName;
		}

		public void setMiddleName(String middleName) {
			this.middleName = middleName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName_LL() {
			return firstName_LL;
		}

		public void setFirstName_LL(String firstName_LL) {
			this.firstName_LL = firstName_LL;
		}

		public String getMiddleName_LL() {
			return middleName_LL;
		}

		public void setMiddleName_LL(String middleName_LL) {
			this.middleName_LL = middleName_LL;
		}

		public String getLastName_LL() {
			return lastName_LL;
		}

		public void setLastName_LL(String lastName_LL) {
			this.lastName_LL = lastName_LL;
		}

		public String getCandidateMobile() {
			return candidateMobile;
		}

		public void setCandidateMobile(String candidateMobile) {
			this.candidateMobile = candidateMobile;
		}

		public String getCandidateEmail() {
			return candidateEmail;
		}

		public void setCandidateEmail(String candidateEmail) {
			this.candidateEmail = candidateEmail;
		}

		public String getGender() {
			return gender;
		}

		public void setGender(String gender) {
			this.gender = gender;
		}

		public String getDob() {
			return dob;
		}

		public void setDob(String dob) {
			this.dob = dob;
		}

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}

		public int getOccupation() {
			return occupation;
		}

		public void setOccupation(int occupation) {
			this.occupation = occupation;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getPin() {
			return pin;
		}

		public void setPin(String pin) {
			this.pin = pin;
		}

		public String getCategory() {
			return category;
		}

		public void setCategory(String category) {
			this.category = category;
		}

		public String getCaste() {
			return caste;
		}

		public void setCaste(String caste) {
			this.caste = caste;
		}

		public String getWomenReservedSeat() {
			return womenReservedSeat;
		}

		public void setWomenReservedSeat(String womenReservedSeat) {
			this.womenReservedSeat = womenReservedSeat;
		}

		public String getCandidature() {
			return candidature;
		}

		public void setCandidature(String candidature) {
			this.candidature = candidature;
		}

		
		
		public int getPartyCategoryId() {
			return partyCategoryId;
		}

		public void setPartyCategoryId(int partyCategoryId) {
			this.partyCategoryId = partyCategoryId;
		}

		public int getNameOfPartyId() {
			return nameOfPartyId;
		}

		public void setNameOfPartyId(int nameOfPartyId) {
			this.nameOfPartyId = nameOfPartyId;
		}


		
		
		public int getSymbolChoice1() {
			return symbolChoice1;
		}

		public void setSymbolChoice1(int symbolChoice1) {
			this.symbolChoice1 = symbolChoice1;
		}

		public int getSymbolChoice2() {
			return symbolChoice2;
		}

		public void setSymbolChoice2(int symbolChoice2) {
			this.symbolChoice2 = symbolChoice2;
		}

		public int getSymbolChoice3() {
			return symbolChoice3;
		}

		public void setSymbolChoice3(int symbolChoice3) {
			this.symbolChoice3 = symbolChoice3;
		}

		public int getCandidateWardNo() {
			return candidateWardNo;
		}

		public void setCandidateWardNo(int candidateWardNo) {
			this.candidateWardNo = candidateWardNo;
		}

		public String getCandidateSerialNo() {
			return candidateSerialNo;
		}

		public void setCandidateSerialNo(String candidateSerialNo) {
			this.candidateSerialNo = candidateSerialNo;
		}

		public String getProposerFirstName1() {
			return proposerFirstName1;
		}

		public void setProposerFirstName1(String proposerFirstName1) {
			this.proposerFirstName1 = proposerFirstName1;
		}

		public String getProposerLastName1() {
			return proposerLastName1;
		}

		public void setProposerLastName1(String proposerLastName1) {
			this.proposerLastName1 = proposerLastName1;
		}

		public String getProposerForHName1() {
			return proposerForHName1;
		}

		public void setProposerForHName1(String proposerForHName1) {
			this.proposerForHName1 = proposerForHName1;
		}

		public String getProposerWardNo1() {
			return proposerWardNo1;
		}

		public void setProposerWardNo1(String proposerWardNo1) {
			this.proposerWardNo1 = proposerWardNo1;
		}

		public String getProposerSerialNo1() {
			return proposerSerialNo1;
		}

		public void setProposerSerialNo1(String proposerSerialNo1) {
			this.proposerSerialNo1 = proposerSerialNo1;
		}

		public String getProposerFirstName2() {
			return proposerFirstName2;
		}

		public void setProposerFirstName2(String proposerFirstName2) {
			this.proposerFirstName2 = proposerFirstName2;
		}

		public String getProposerLastName2() {
			return proposerLastName2;
		}

		public void setProposerLastName2(String proposerLastName2) {
			this.proposerLastName2 = proposerLastName2;
		}

		public String getProposerForHName2() {
			return proposerForHName2;
		}

		public void setProposerForHName2(String proposerForHName2) {
			this.proposerForHName2 = proposerForHName2;
		}

		public String getProposerWardNo2() {
			return proposerWardNo2;
		}

		public void setProposerWardNo2(String proposerWardNo2) {
			this.proposerWardNo2 = proposerWardNo2;
		}

		public String getProposerSerialNo2() {
			return proposerSerialNo2;
		}

		public void setProposerSerialNo2(String proposerSerialNo2) {
			this.proposerSerialNo2 = proposerSerialNo2;
		}
		
		
		public String getProposerVoterListPartNo1() {
			return proposerVoterListPartNo1;
		}

		public void setProposerVoterListPartNo1(String proposerVoterListPartNo1) {
			this.proposerVoterListPartNo1 = proposerVoterListPartNo1;
		}

		public String getProposerVoterListPartNo2() {
			return proposerVoterListPartNo2;
		}

		public void setProposerVoterListPartNo2(String proposerVoterListPartNo2) {
			this.proposerVoterListPartNo2 = proposerVoterListPartNo2;
		}

		public String getProposerFirstName3() {
			return proposerFirstName3;
		}

		public void setProposerFirstName3(String proposerFirstName3) {
			this.proposerFirstName3 = proposerFirstName3;
		}

		public String getProposerLastName3() {
			return proposerLastName3;
		}

		public void setProposerLastName3(String proposerLastName3) {
			this.proposerLastName3 = proposerLastName3;
		}

		public String getProposerForHName3() {
			return proposerForHName3;
		}

		public void setProposerForHName3(String proposerForHName3) {
			this.proposerForHName3 = proposerForHName3;
		}

		public String getProposerWardNo3() {
			return proposerWardNo3;
		}

		public void setProposerWardNo3(String proposerWardNo3) {
			this.proposerWardNo3 = proposerWardNo3;
		}

		public String getProposerSerialNo3() {
			return proposerSerialNo3;
		}

		public void setProposerSerialNo3(String proposerSerialNo3) {
			this.proposerSerialNo3 = proposerSerialNo3;
		}

		public String getProposerVoterListPartNo3() {
			return proposerVoterListPartNo3;
		}

		public void setProposerVoterListPartNo3(String proposerVoterListPartNo3) {
			this.proposerVoterListPartNo3 = proposerVoterListPartNo3;
		}

		public String getProposerFirstName4() {
			return proposerFirstName4;
		}

		public void setProposerFirstName4(String proposerFirstName4) {
			this.proposerFirstName4 = proposerFirstName4;
		}

		public String getProposerLastName4() {
			return proposerLastName4;
		}

		public void setProposerLastName4(String proposerLastName4) {
			this.proposerLastName4 = proposerLastName4;
		}

		public String getProposerForHName4() {
			return proposerForHName4;
		}

		public void setProposerForHName4(String proposerForHName4) {
			this.proposerForHName4 = proposerForHName4;
		}

		public String getProposerWardNo4() {
			return proposerWardNo4;
		}

		public void setProposerWardNo4(String proposerWardNo4) {
			this.proposerWardNo4 = proposerWardNo4;
		}

		public String getProposerSerialNo4() {
			return proposerSerialNo4;
		}

		public void setProposerSerialNo4(String proposerSerialNo4) {
			this.proposerSerialNo4 = proposerSerialNo4;
		}

		public String getProposerVoterListPartNo4() {
			return proposerVoterListPartNo4;
		}

		public void setProposerVoterListPartNo4(String proposerVoterListPartNo4) {
			this.proposerVoterListPartNo4 = proposerVoterListPartNo4;
		}

		public String getProposerFirstName5() {
			return proposerFirstName5;
		}

		public void setProposerFirstName5(String proposerFirstName5) {
			this.proposerFirstName5 = proposerFirstName5;
		}

		public String getProposerLastName5() {
			return proposerLastName5;
		}

		public void setProposerLastName5(String proposerLastName5) {
			this.proposerLastName5 = proposerLastName5;
		}

		public String getProposerForHName5() {
			return proposerForHName5;
		}

		public void setProposerForHName5(String proposerForHName5) {
			this.proposerForHName5 = proposerForHName5;
		}

		public String getProposerWardNo5() {
			return proposerWardNo5;
		}

		public void setProposerWardNo5(String proposerWardNo5) {
			this.proposerWardNo5 = proposerWardNo5;
		}

		public String getProposerSerialNo5() {
			return proposerSerialNo5;
		}

		public void setProposerSerialNo5(String proposerSerialNo5) {
			this.proposerSerialNo5 = proposerSerialNo5;
		}

		public String getProposerVoterListPartNo5() {
			return proposerVoterListPartNo5;
		}

		public void setProposerVoterListPartNo5(String proposerVoterListPartNo5) {
			this.proposerVoterListPartNo5 = proposerVoterListPartNo5;
		}

		public String getWillingness() {
			return willingness;
		}

		public void setWillingness(String willingness) {
			this.willingness = willingness;
		}

		public String getRdb_AgeValidation() {
			return rdb_AgeValidation;
		}

		public void setRdb_AgeValidation(String rdb_AgeValidation) {
			this.rdb_AgeValidation = rdb_AgeValidation;
		}

		public String getRdb_EligibleElection() {
			return rdb_EligibleElection;
		}

		public void setRdb_EligibleElection(String rdb_EligibleElection) {
			this.rdb_EligibleElection = rdb_EligibleElection;
		}

		public String getRdb_WhetherContractor() {
			return rdb_WhetherContractor;
		}

		public void setRdb_WhetherContractor(String rdb_WhetherContractor) {
			this.rdb_WhetherContractor = rdb_WhetherContractor;
		}

		public String getRdb_OutStanreg_Typeding() {
			return rdb_OutStanding;
		}

		public void setRdb_OutStanding(String rdb_OutStanding) {
			this.rdb_OutStanding = rdb_OutStanding;
		}

		public String getReservedCategory() {
			return reservedCategory;
		}

		public void setReservedCategory(String reservedCategory) {
			this.reservedCategory = reservedCategory;
		}

		public String getCreatedBy() {
			return createdBy;
		}

		public void setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
		}

		public String getCreatedDate() {
			return createdDate;
		}

		public void setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
		}

		public String getElectionProgName() {
			return electionProgName;
		}

		public void setElectionProgName(String electionProgName) {
			this.electionProgName = electionProgName;
		}

		public String getLocalBodyName() {
			return localBodyName;
		}

		public void setLocalBodyName(String localBodyName) {
			this.localBodyName = localBodyName;
		}

		public int getReportType() {
			return reportType;
		}

		public void setReportType(int reportType) {
			this.reportType = reportType;
		}

		public int getSeatId() {
			return seatId;
		}

		public void setSeatId(int seatId) {
			this.seatId = seatId;
		}
		
		
//		@Transient
//		@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
//		private Date modifiedDate;
	
		
		
}