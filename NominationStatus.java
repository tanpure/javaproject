package com.nomination.model;

public class NominationStatus {
	private String electionProgram;
	private int statusPending;
	private int statusAccepted;
	private int statusRejected;
	public String getElectionProgram() {
		return electionProgram;
	}
	public void setElectionProgram(String electionProgram) {
		this.electionProgram = electionProgram;
	}
	public int getStatusPending() {
		return statusPending;
	}
	public void setStatusPending(int statusPending) {
		this.statusPending = statusPending;
	}
	public int getStatusAccepted() {
		return statusAccepted;
	}
	public void setStatusAccepted(int statusAccepted) {
		this.statusAccepted = statusAccepted;
	}
	public int getStatusRejected() {
		return statusRejected;
	}
	public void setStatusRejected(int statusRejected) {
		this.statusRejected = statusRejected;
	}

	
	
}
