package com.nomination.model;

import java.io.Serializable;

public class PartyDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int partyId;
	private int partyCatId;
	private String partyCategory;
	private String partyName;
	private String partyName_LL;
	private int symbolID;
	private int categoryID;
	private String formType;
	private String symbolName;
	private String symbolName_LL;
	private int active;
	private String status;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	public int getPartyId() {
		return partyId;
	}
	public void setPartyId(int partyId) {
		this.partyId = partyId;
	}
	public int getPartyCatId() {
		return partyCatId;
	}
	public void setPartyCatId(int partyCatId) {
		this.partyCatId = partyCatId;
	}
	public String getPartyCategory() {
		return partyCategory;
	}
	public void setPartyCategory(String partyCategory) {
		this.partyCategory = partyCategory;
	}
	public String getPartyName() {
		return partyName;
	}
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}
	public String getPartyName_LL() {
		return partyName_LL;
	}
	public void setPartyName_LL(String partyName_LL) {
		this.partyName_LL = partyName_LL;
	}
	public int getSymbolID() {
		return symbolID;
	}
	public void setSymbolID(int symbolID) {
		this.symbolID = symbolID;
	}
	public int getCategoryID() {
		return categoryID;
	}
	public void setCategoryID(int categoryID) {
		this.categoryID = categoryID;
	}
	public String getFormType() {
		return formType;
	}
	public void setFormType(String formType) {
		this.formType = formType;
	}
	public String getSymbolName() {
		return symbolName;
	}
	public void setSymbolName(String symbolName) {
		this.symbolName = symbolName;
	}
	public String getSymbolName_LL() {
		return symbolName_LL;
	}
	public void setSymbolName_LL(String symbolName_LL) {
		this.symbolName_LL = symbolName_LL;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
