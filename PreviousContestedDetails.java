package com.nomination.model;

import java.io.Serializable;

public class PreviousContestedDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int previousContestedId;
	private int affId;
	private String previousCheck;
	private String nameOfElection;
	private String yearOfElection;
	private double totalAssetValue;
	private String createdBy;
	private String modifiedBy;
	private String uId;
	private int isSubmitted;
	private int isApprove;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getPreviousContestedId() {
		return previousContestedId;
	}
	public void setPreviousContestedId(int previousContestedId) {
		this.previousContestedId = previousContestedId;
	}
	public int getAffId() {
		return affId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public String getPreviousCheck() {
		return previousCheck;
	}
	public void setPreviousCheck(String previousCheck) {
		this.previousCheck = previousCheck;
	}
	public String getNameOfElection() {
		return nameOfElection;
	}
	public void setNameOfElection(String nameOfElection) {
		this.nameOfElection = nameOfElection;
	}
	public String getYearOfElection() {
		return yearOfElection;
	}
	public void setYearOfElection(String yearOfElection) {
		this.yearOfElection = yearOfElection;
	}
	public double getTotalAssetValue() {
		return totalAssetValue;
	}
	public void setTotalAssetValue(double totalAssetValue) {
		this.totalAssetValue = totalAssetValue;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
