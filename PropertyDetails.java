package com.nomination.model;

import java.io.Serializable;

public class PropertyDetails implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int propId;
	private int propertyType;
	private String surname;
	private String firstName;
	private String fandHName;
	private double cashInHand;
	private String otherProperty;
	private int affId;
	private String uId;
	private String createdBy;
	private String modifiedBy;
	private int isSubmitted;
	private int isApprove;
	private int fdId;
	private String fdBankName;
	private int fdAmount;
	private int cId;
	private int sharesId;
	private int shareType;
	private String companyName;
	private double noOfShares;
	private int vId;
	private int vType; 
	private String vRegNo;
	private String vManfac;
	private double vPurchase;
	private int jId;
	private int jType;
	private double jWeight;
	private double jValue;
	private int lId;
	private int lPoliciesTypes;
	private double lSumAsured;
	private int immovablePropertyId;
	private String village;
	private String surveyNumber;
	private String area;
	private String currentMarketValue;
	private int immovablePropertyType;
	private int candidateId;
	private String costDate;
	private String cost;
	private String selfAcquiredProperty;
	private int isStatus;
	private String token;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getPropId() {
		return propId;
	}
	public void setPropId(int propId) {
		this.propId = propId;
	}
	public int getPropertyType() {
		return propertyType;
	}
	public void setPropertyType(int propertyType) {
		this.propertyType = propertyType;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getFandHName() {
		return fandHName;
	}
	public void setFandHName(String fandHName) {
		this.fandHName = fandHName;
	}
	public double getCashInHand() {
		return cashInHand;
	}
	public void setCashInHand(double cashInHand) {
		this.cashInHand = cashInHand;
	}
	public String getOtherProperty() {
		return otherProperty;
	}
	public void setOtherProperty(String otherProperty) {
		this.otherProperty = otherProperty;
	}
	public int getAffId() {
		return affId;
	}
	public void setAffId(int affId) {
		this.affId = affId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getIsSubmitted() {
		return isSubmitted;
	}
	public void setIsSubmitted(int isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
	public int getIsApprove() {
		return isApprove;
	}
	public void setIsApprove(int isApprove) {
		this.isApprove = isApprove;
	}
	public int getFdId() {
		return fdId;
	}
	public void setFdId(int fdId) {
		this.fdId = fdId;
	}
	public String getFdBankName() {
		return fdBankName;
	}
	public void setFdBankName(String fdBankName) {
		this.fdBankName = fdBankName;
	}
	public int getFdAmount() {
		return fdAmount;
	}
	public void setFdAmount(int fdAmount) {
		this.fdAmount = fdAmount;
	}
	public int getcId() {
		return cId;
	}
	public void setcId(int cId) {
		this.cId = cId;
	}
	public int getSharesId() {
		return sharesId;
	}
	public void setSharesId(int sharesId) {
		this.sharesId = sharesId;
	}
	public int getShareType() {
		return shareType;
	}
	public void setShareType(int shareType) {
		this.shareType = shareType;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public double getNoOfShares() {
		return noOfShares;
	}
	public void setNoOfShares(double noOfShares) {
		this.noOfShares = noOfShares;
	}
	public int getvId() {
		return vId;
	}
	public void setvId(int vId) {
		this.vId = vId;
	}
	public int getvType() {
		return vType;
	}
	public void setvType(int vType) {
		this.vType = vType;
	}
	public String getvRegNo() {
		return vRegNo;
	}
	public void setvRegNo(String vRegNo) {
		this.vRegNo = vRegNo;
	}
	public String getvManfac() {
		return vManfac;
	}
	public void setvManfac(String vManfac) {
		this.vManfac = vManfac;
	}
	public double getvPurchase() {
		return vPurchase;
	}
	public void setvPurchase(double vPurchase) {
		this.vPurchase = vPurchase;
	}
	public int getjId() {
		return jId;
	}
	public void setjId(int jId) {
		this.jId = jId;
	}
	public int getjType() {
		return jType;
	}
	public void setjType(int jType) {
		this.jType = jType;
	}
	public double getjWeight() {
		return jWeight;
	}
	public void setjWeight(double jWeight) {
		this.jWeight = jWeight;
	}
	public double getjValue() {
		return jValue;
	}
	public void setjValue(double jValue) {
		this.jValue = jValue;
	}
	public int getlId() {
		return lId;
	}
	public int getlPoliciesTypes() {
		return lPoliciesTypes;
	}
	public double getlSumAsured() {
		return lSumAsured;
	}
	public void setlId(int lId) {
		this.lId = lId;
	}
	public void setlPoliciesTypes(int lPoliciesTypes) {
		this.lPoliciesTypes = lPoliciesTypes;
	}
	public void setlSumAsured(double lSumAsured) {
		this.lSumAsured = lSumAsured;
	}
	public int getImmovablePropertyId() {
		return immovablePropertyId;
	}
	public String getVillage() {
		return village;
	}
	public String getSurveyNumber() {
		return surveyNumber;
	}
	public String getArea() {
		return area;
	}
	public String getCurrentMarketValue() {
		return currentMarketValue;
	}
	public int getImmovablePropertyType() {
		return immovablePropertyType;
	}
	public int getCandidateId() {
		return candidateId;
	}
	public String getCostDate() {
		return costDate;
	}
	public String getCost() {
		return cost;
	}
	public String getSelfAcquiredProperty() {
		return selfAcquiredProperty;
	}
	public void setImmovablePropertyId(int immovablePropertyId) {
		this.immovablePropertyId = immovablePropertyId;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public void setSurveyNumber(String surveyNumber) {
		this.surveyNumber = surveyNumber;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public void setCurrentMarketValue(String currentMarketValue) {
		this.currentMarketValue = currentMarketValue;
	}
	public void setImmovablePropertyType(int immovablePropertyType) {
		this.immovablePropertyType = immovablePropertyType;
	}
	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}
	public void setCostDate(String costDate) {
		this.costDate = costDate;
	}
	public void setCost(String cost) {
		this.cost = cost;
	}
	public void setSelfAcquiredProperty(String selfAcquiredProperty) {
		this.selfAcquiredProperty = selfAcquiredProperty;
	}
	public int getIsStatus() {
		return isStatus;
	}
	public void setIsStatus(int isStatus) {
		this.isStatus = isStatus;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
