package com.nomination.model;

public class ReligionMasterModel {

	private int rId;
	private String religionName;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getrId() {
		return rId;
	}
	public void setrId(int rId) {
		this.rId = rId;
	}
	public String getReligionName() {
		return religionName;
	}
	public void setReligionName(String religionName) {
		this.religionName = religionName;
	}
	
	
}
