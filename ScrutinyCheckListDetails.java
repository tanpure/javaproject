package com.nomination.model;

import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ScrutinyCheckListDetails {
	
	private int id;
	@Transient
	private String electionProgram;
	@Transient
	private int localBodyID;
	private String rdbApplicantName;
	private String rdbApplicantNameInSamePart;
	private String rdbSignedByCand;
	private String rdbAmountPaid;
	private String rdbCasteCertificate;
	private String rdbCasteValidity;
	private String rdbCriminalRecordSigned;
	private String rdbSmallFamilyDeclared;
	private String rdbIdentityProof;
	private String rdbResidenceProof;
	private String rdbNoDueCertificate;
	private String rdbAgeProof;
	private String rdbNotContractor;
	private String rdbToilet;
	private String rdbMoreThan2Child;
	private String noOfForms;
	private String objectionRecieved;
	private String overruledOrSustained;
	private String reasonForOverRuled;
	private String rdbScrunityDecision;
	private String reasonOfRejection;
	private String nominationId;
	private String su_ROID;
	private String electionProgramName;
    private String localBodyId;
	public String getElectionProgramName() {
		return electionProgramName;
	}

	public void setElectionProgramName(String electionProgramName) {
		this.electionProgramName = electionProgramName;
	}

	public String getLocalBodyId() {
		return localBodyId;
	}

	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	
	@Transient
	@JsonFormat(shape=JsonFormat.Shape.ANY, pattern="yyyy-MM-dd HH:mm:ss")
	private String createdDate;
	
	@Transient
	@JsonFormat(shape=JsonFormat.Shape.ANY, pattern="yyyy-MM-dd HH:mm:ss")
	private String modifiedDate;

	
	
	public String getElectionProgram() {
		return electionProgram;
	}

	public void setElectionProgram(String electionProgram) {
		this.electionProgram = electionProgram;
	}

	public int getLocalBodyID() {
		return localBodyID;
	}

	public void setLocalBodyID(int localBodyID) {
		this.localBodyID = localBodyID;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRdbApplicantName() {
		return rdbApplicantName;
	}

	public void setRdbApplicantName(String rdbApplicantName) {
		this.rdbApplicantName = rdbApplicantName;
	}

	public String getRdbApplicantNameInSamePart() {
		return rdbApplicantNameInSamePart;
	}

	public void setRdbApplicantNameInSamePart(String rdbApplicantNameInSamePart) {
		this.rdbApplicantNameInSamePart = rdbApplicantNameInSamePart;
	}

	public String getRdbSignedByCand() {
		return rdbSignedByCand;
	}

	public void setRdbSignedByCand(String rdbSignedByCand) {
		this.rdbSignedByCand = rdbSignedByCand;
	}

	public String getRdbAmountPaid() {
		return rdbAmountPaid;
	}

	public void setRdbAmountPaid(String rdbAmountPaid) {
		this.rdbAmountPaid = rdbAmountPaid;
	}

	public String getRdbCasteCertificate() {
		return rdbCasteCertificate;
	}

	public void setRdbCasteCertificate(String rdbCasteCertificate) {
		this.rdbCasteCertificate = rdbCasteCertificate;
	}

	public String getRdbCasteValidity() {
		return rdbCasteValidity;
	}

	public void setRdbCasteValidity(String rdbCasteValidity) {
		this.rdbCasteValidity = rdbCasteValidity;
	}

	public String getRdbCriminalRecordSigned() {
		return rdbCriminalRecordSigned;
	}

	public void setRdbCriminalRecordSigned(String rdbCriminalRecordSigned) {
		this.rdbCriminalRecordSigned = rdbCriminalRecordSigned;
	}

	public String getRdbSmallFamilyDeclared() {
		return rdbSmallFamilyDeclared;
	}

	public void setRdbSmallFamilyDeclared(String rdbSmallFamilyDeclared) {
		this.rdbSmallFamilyDeclared = rdbSmallFamilyDeclared;
	}

	public String getRdbIdentityProof() {
		return rdbIdentityProof;
	}

	public void setRdbIdentityProof(String rdbIdentityProof) {
		this.rdbIdentityProof = rdbIdentityProof;
	}

	public String getRdbResidenceProof() {
		return rdbResidenceProof;
	}

	public void setRdbResidenceProof(String rdbResidenceProof) {
		this.rdbResidenceProof = rdbResidenceProof;
	}

	public String getRdbNoDueCertificate() {
		return rdbNoDueCertificate;
	}

	public void setRdbNoDueCertificate(String rdbNoDueCertificate) {
		this.rdbNoDueCertificate = rdbNoDueCertificate;
	}

	public String getRdbAgeProof() {
		return rdbAgeProof;
	}

	public void setRdbAgeProof(String rdbAgeProof) {
		this.rdbAgeProof = rdbAgeProof;
	}

	public String getRdbNotContractor() {
		return rdbNotContractor;
	}

	public void setRdbNotContractor(String rdbNotContractor) {
		this.rdbNotContractor = rdbNotContractor;
	}

	public String getRdbToilet() {
		return rdbToilet;
	}

	public void setRdbToilet(String rdbToilet) {
		this.rdbToilet = rdbToilet;
	}

	public String getRdbMoreThan2Child() {
		return rdbMoreThan2Child;
	}

	public void setRdbMoreThan2Child(String rdbMoreThan2Child) {
		this.rdbMoreThan2Child = rdbMoreThan2Child;
	}

	public String getNoOfForms() {
		return noOfForms;
	}

	public void setNoOfForms(String noOfForms) {
		this.noOfForms = noOfForms;
	}

	public String getObjectionRecieved() {
		return objectionRecieved;
	}

	public void setObjectionRecieved(String objectionRecieved) {
		this.objectionRecieved = objectionRecieved;
	}

	public String getOverruledOrSustained() {
		return overruledOrSustained;
	}

	public void setOverruledOrSustained(String overruledOrSustained) {
		this.overruledOrSustained = overruledOrSustained;
	}

	public String getReasonForOverRuled() {
		return reasonForOverRuled;
	}

	public void setReasonForOverRuled(String reasonForOverRuled) {
		this.reasonForOverRuled = reasonForOverRuled;
	}

	public String getRdbScrunityDecision() {
		return rdbScrunityDecision;
	}

	public void setRdbScrunityDecision(String rdbScrunityDecision) {
		this.rdbScrunityDecision = rdbScrunityDecision;
	}

	public String getReasonOfRejection() {
		return reasonOfRejection;
	}

	public void setReasonOfRejection(String reasonOfRejection) {
		this.reasonOfRejection = reasonOfRejection;
	}

	public String getNominationId() {
		return nominationId;
	}

	public void setNominationId(String nominationId) {
		this.nominationId = nominationId;
	}

	public String getSu_ROID() {
		return su_ROID;
	}

	public void setSu_ROID(String su_ROID) {
		this.su_ROID = su_ROID;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	

}
