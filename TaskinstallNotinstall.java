package com.nomination.model;

public class TaskinstallNotinstall {
	private int acNo;
	private int partNo;
	private int epId;
	private int install;
	private int notinstall;
	
	
	
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getAcNo() {
		return acNo;
		
	}
	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}
	public int getPartNo() {
		return partNo;
	}
	public void setPartNo(int partNo) {
		this.partNo = partNo;
	}
	public int getInstallapp() {
		return install;
	}
	public void setInstallapp(int installapp) {
		this.install = installapp;
	}
	public int getNotinstallapp() {
		return notinstall;
	}
	public void setNotinstallapp(int notinstallapp) {
		this.notinstall = notinstallapp;
	}
	

}
