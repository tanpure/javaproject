package com.nomination.model;

public class TotalSubExpense {
		private int epId;
		//private int wardNo;
		private int expenseId;
		private String firstName;
		private String lastName;
		private String userMobileNo;
		//private String createdBy;
		//private String uId;
		private double totalExpense;
		private String localBodyId;
		private String nominationId;
		private int subExpenseId;
		public int getEpId() {
			return epId;
		}
		public void setEpId(int epId) {
			this.epId = epId;
		}
		public int getExpenseId() {
			return expenseId;
		}
		public void setExpenseId(int expenseId) {
			this.expenseId = expenseId;
		}
		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getUserMobileNo() {
			return userMobileNo;
		}
		public void setUserMobileNo(String userMobileNo) {
			this.userMobileNo = userMobileNo;
		}
		public double getTotalExpense() {
			return totalExpense;
		}
		public void setTotalExpense(double totalExpense) {
			this.totalExpense = totalExpense;
		}
		public String getLocalBodyId() {
			return localBodyId;
		}
		public void setLocalBodyId(String localBodyId) {
			this.localBodyId = localBodyId;
		}
		public String getNominationId() {
			return nominationId;
		}
		public void setNominationId(String nominationId) {
			this.nominationId = nominationId;
		}
		public int getSubExpenseId() {
			return subExpenseId;
		}
		public void setSubExpenseId(int subExpenseId) {
			this.subExpenseId = subExpenseId;
		}
		
		
		
		
}	