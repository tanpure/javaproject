package com.nomination.model;

public class UpdateVerifyPojo {

		private int id;
		private int acNo;
		private int partNo;
		private int localBodyId;
		private String isCorrect;
		private String correctedBy;
		private String i_Uid;
		private String i_status;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getAcNo() {
			return acNo;
		}
		public void setAcNo(int acNo) {
			this.acNo = acNo;
		}
		public int getPartNo() {
			return partNo;
		}
		public void setPartNo(int partNo) {
			this.partNo = partNo;
		}
		public int getLocalBodyId() {
			return localBodyId;
		}
		public void setLocalBodyId(int localBodyId) {
			this.localBodyId = localBodyId;
		}
		public String getIsCorrect() {
			return isCorrect;
		}
		public void setIsCorrect(String isCorrect) {
			this.isCorrect = isCorrect;
		}
		public String getCorrectedBy() {
			return correctedBy;
		}
		public void setCorrectedBy(String correctedBy) {
			this.correctedBy = correctedBy;
		}
		public String getI_Uid() {
			return i_Uid;
		}
		public void setI_Uid(String i_Uid) {
			this.i_Uid = i_Uid;
		}
		public String getI_status() {
			return i_status;
		}
		public void setI_status(String i_status) {
			this.i_status = i_status;
		}

		
	}
