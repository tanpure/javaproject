package com.nomination.model;

public class VoterObjectionApprovedPojo {
	private int id;
	private String mobileNo;
	private String name;
	private String emailId;
	private String uId;
	private int localBodyId;
	private int acNo;
	private int partNo;
	private int srNo;
	private int expectedWardNo;
	private String voterName;
	private int objectionType;
	private String address;
	private String isCorrect;
	private String correctedBy;
	private String isApproved;
	private String approvedBy;
	private String objectionDate;
	//private int status;
	private String token;
	private String createdBy;
	private String modifiedBy;
	private int districtId;
	private int epId;
	private int localBodyType;
	private int istatus;
	
	
	
	public int getIstatus() {
		return istatus;
	}
	public void setIstatus(int istatus) {
		this.istatus = istatus;
	}
	public int getLocalBodyType() {
		return localBodyType;
	}
	public void setLocalBodyType(int localBodyType) {
		this.localBodyType = localBodyType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getAcNo() {
		return acNo;
	}
	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}
	public int getPartNo() {
		return partNo;
	}
	public void setPartNo(int partNo) {
		this.partNo = partNo;
	}
	public int getSrNo() {
		return srNo;
	}
	public void setSrNo(int srNo) {
		this.srNo = srNo;
	}
	public int getExpectedWardNo() {
		return expectedWardNo;
	}
	public void setExpectedWardNo(int expectedWardNo) {
		this.expectedWardNo = expectedWardNo;
	}
	public String getVoterName() {
		return voterName;
	}
	public void setVoterName(String voterName) {
		this.voterName = voterName;
	}
	public int getObjectionType() {
		return objectionType;
	}
	public void setObjectionType(int objectionType) {
		this.objectionType = objectionType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(String isCorrect) {
		this.isCorrect = isCorrect;
	}
	public String getCorrectedBy() {
		return correctedBy;
	}
	public void setCorrectedBy(String correctedBy) {
		this.correctedBy = correctedBy;
	}
	public String getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(String isApproved) {
		this.isApproved = isApproved;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getObjectionDate() {
		return objectionDate;
	}
	public void setObjectionDate(String objectionDate) {
		this.objectionDate = objectionDate;
	}
//	public int getStatus() {
	//	return status;
//	}
	//public void setStatus(int status) {
	//	this.status = status;
	//}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getDistrictId() {
		return districtId;
	}
	public void setDistrictId(int districtId) {
		this.districtId = districtId;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}

	
	
}
