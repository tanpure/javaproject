package com.nomination.model;

public class VotingObjectionPojo {
	
	private int distId;
	private int localBodyType;
	private String localBodyId;
	private int epId;
	private int wardNo;
	private int id;
	private String qty_Size_Area;
	private String standardRate;
	private String noticeDate;
	private String visitDate;
	private String status;
	private String firstName;
	private String lastName;
	private String userMobileNo;
	private int countobjection;
	
	
	public int getCountobjection() {
		return countobjection;
	}
	public void setCountobjection(int countobjection) {
		this.countobjection = countobjection;
	}
	public int getDistId() {
		return distId;
	}
	public void setDistId(int distId) {
		this.distId = distId;
	}
	public int getLocalBodyType() {
		return localBodyType;
	}
	public void setLocalBodyType(int localBodyType) {
		this.localBodyType = localBodyType;
	}
	public String getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(String localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getQty_Size_Area() {
		return qty_Size_Area;
	}
	public void setQty_Size_Area(String qty_Size_Area) {
		this.qty_Size_Area = qty_Size_Area;
	}
	public String getStandardRate() {
		return standardRate;
	}
	public void setStandardRate(String standardRate) {
		this.standardRate = standardRate;
	}
	public String getNoticeDate() {
		return noticeDate;
	}
	public void setNoticeDate(String noticeDate) {
		this.noticeDate = noticeDate;
	}
	public String getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(String visitDate) {
		this.visitDate = visitDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserMobileNo() {
		return userMobileNo;
	}
	public void setUserMobileNo(String userMobileNo) {
		this.userMobileNo = userMobileNo;
	}
	

}
