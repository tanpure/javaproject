package com.nomination.model;

public class WorkdoneCountPojo {
	private String count0_20;
	private String count21_40;
	private String count41_60;
	private String count61_80;
	private String count81_100;

	public String getCount0_20() {
		return count0_20;
	}
	public void setCount0_20(String count0_20) {
		this.count0_20 = count0_20;
	}
	public String getCount21_40() {
		return count21_40;
	}
	public void setCount21_40(String count21_40) {
		this.count21_40 = count21_40;
	}
	public String getCount41_60() {
		return count41_60;
	}
	public void setCount41_60(String count41_60) {
		this.count41_60 = count41_60;
	}
	public String getCount61_80() {
		return count61_80;
	}
	public void setCount61_80(String count61_80) {
		this.count61_80 = count61_80;
	}
	public String getCount81_100() {
		return count81_100;
	}
	public void setCount81_100(String count81_100) {
		this.count81_100 = count81_100;
	}
	
	

}
