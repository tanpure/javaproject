package com.nomination.model;

public class dailyExpensepojo {
	private String date;
	private String localBodyID;
	private int epId;
	private int printed;
	//private int isPrinted;
	private int notprinted;
	
	
		public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLocalBodyID() {
		return localBodyID;
	}
	public void setLocalBodyID(String localBodyID) {
		this.localBodyID = localBodyID;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getPrinted() {
		return printed;
	}
	public void setPrinted(int printed) {
		this.printed = printed;
	}
	public int getNotprinted() {
		return notprinted;
	}
	public void setNotprinted(int notprinted) {
		this.notprinted = notprinted;
	}
	
	

	
	
}
