package com.nomination.model;

public class dateComparePojo {
	private String printDate;
	private int localBodyId;
	private int wardNo;
	private String firstName;
	private String middleName;
	private String lastName;
	private int userMobileNo;
	private String expenseDate;
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getUserMobileNo() {
		return userMobileNo;
	}
	public void setUserMobileNo(int userMobileNo) {
		this.userMobileNo = userMobileNo;
	}
	public String getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}

	
	
	
}
