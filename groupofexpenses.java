package com.nomination.model;

public class groupofexpenses {
	
	
	private int id;
	private String programname;
	
	private String programdate;
	private String member1;
	private double share1;
	private int status1;
	private String member2;
	private double share2;
	private int status2;
	private String member3;
	private double share3;
	private int status3;
	private String member4;
	private double share4;
	private int status4;
	private int enterycount;
	private double total;
	private String uid;
	private int isactive;
	private int expenseid;
	private double standardrate;
	private int unit;
	private String description;
	private int quantity;
	private double rate;
	private String paymenttype;
	private String paymentmode;
	private double amount;
	private double balancepayment;
	private String vendername;
	private String vendermobileno;
	private int billno;
	private String sourceofexpense;
	private int subexpenseid;
	private int localBodyId;
	private int epId;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProgramname() {
		return programname;
	}
	public void setProgramname(String programname) {
		this.programname = programname;
	}
	public String getProgramdate() {
		return programdate;
	}
	public void setProgramdate(String programdate) {
		this.programdate = programdate;
	}
	public String getMember1() {
		return member1;
	}
	public void setMember1(String member1) {
		this.member1 = member1;
	}
	public double getShare1() {
		return share1;
	}
	public void setShare1(double share1) {
		this.share1 = share1;
	}
	public int getStatus1() {
		return status1;
	}
	public void setStatus1(int status1) {
		this.status1 = status1;
	}
	public String getMember2() {
		return member2;
	}
	public void setMember2(String member2) {
		this.member2 = member2;
	}
	public double getShare2() {
		return share2;
	}
	public void setShare2(double share2) {
		this.share2 = share2;
	}
	public int getStatus2() {
		return status2;
	}
	public void setStatus2(int status2) {
		this.status2 = status2;
	}
	public String getMember3() {
		return member3;
	}
	public void setMember3(String member3) {
		this.member3 = member3;
	}
	public double getShare3() {
		return share3;
	}
	public void setShare3(double share3) {
		this.share3 = share3;
	}
	public int getStatus3() {
		return status3;
	}
	public void setStatus3(int status3) {
		this.status3 = status3;
	}
	public String getMember4() {
		return member4;
	}
	public void setMember4(String member4) {
		this.member4 = member4;
	}
	public double getShare4() {
		return share4;
	}
	public void setShare4(double share4) {
		this.share4 = share4;
	}
	public int getStatus4() {
		return status4;
	}
	public void setStatus4(int status4) {
		this.status4 = status4;
	}
	public int getEnterycount() {
		return enterycount;
	}
	public void setEnterycount(int enterycount) {
		this.enterycount = enterycount;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public int getIsactive() {
		return isactive;
	}
	public void setIsactive(int isactive) {
		this.isactive = isactive;
	}
	public int getExpenseid() {
		return expenseid;
	}
	public void setExpenseid(int expenseid) {
		this.expenseid = expenseid;
	}
	public double getStandardrate() {
		return standardrate;
	}
	public void setStandardrate(double standardrate) {
		this.standardrate = standardrate;
	}
	public int getUnit() {
		return unit;
	}
	public void setUnit(int unit) {
		this.unit = unit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public String getPaymenttype() {
		return paymenttype;
	}
	public void setPaymenttype(String paymenttype) {
		this.paymenttype = paymenttype;
	}
	public String getPaymentmode() {
		return paymentmode;
	}
	public void setPaymentmode(String paymentmode) {
		this.paymentmode = paymentmode;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getBalancepayment() {
		return balancepayment;
	}
	public void setBalancepayment(double balancepayment) {
		this.balancepayment = balancepayment;
	}
	public String getVendername() {
		return vendername;
	}
	public void setVendername(String vendername) {
		this.vendername = vendername;
	}

	public String getVendermobileno() {
		return vendermobileno;
	}
	public void setVendermobileno(String vendermobileno) {
		this.vendermobileno = vendermobileno;
	}
	public int getBillno() {
		return billno;
	}
	public void setBillno(int billno) {
		this.billno = billno;
	}
	public String getSourceofexpense() {
		return sourceofexpense;
	}
	public void setSourceofexpense(String sourceofexpense) {
		this.sourceofexpense = sourceofexpense;
	}
	
	public int getSubexpenseid() {
		return subexpenseid;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public void setSubexpenseid(int subexpenseid) {
		this.subexpenseid = subexpenseid;
	}
	

}