package com.nomination.model;

public class logwithcandidatepojo {
	private String printedBy;
	private String printDate;
	private int localBodyId;
	private String expenseDate;
	private int wardNo;
	private String mobileNo;
	private String uId;
	
	public String getPrintedBy() {
		return printedBy;
	}
	public void setPrintedBy(String printedBy) {
		this.printedBy = printedBy;
	}
	public String getPrintDate() {
		return printDate;
	}
	public void setPrintDate(String printDate) {
		this.printDate = printDate;
	}
	public int getLocalBodyId() {
		return localBodyId;
	}
	public void setLocalBodyId(int localBodyId) {
		this.localBodyId = localBodyId;
	}
	public String getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}
	public int getWardNo() {
		return wardNo;
	}
	public void setWardNo(int wardNo) {
		this.wardNo = wardNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public String getuId() {
		return uId;
	}
	public void setuId(String uId) {
		this.uId = uId;
	}

	
}
