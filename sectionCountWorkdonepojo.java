package com.nomination.model;

public class sectionCountWorkdonepojo {
	private int id;
	private int acNo;
	private int partNo;
	private int epId;
	private int sectionCount;
	private String voterCount;
	private int newSectionCount;
	private int detailsUpdated;
	private String createdBy;
	private String modifiedBy;
	private String modifiedDate;
	private String createdDate;
	private double workdone;
	
	
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public int getPartNo() {
		return partNo;
	}
	public void setPartNo(int partNo) {
		this.partNo = partNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAcNo() {
		return acNo;
	}
	public void setAcNo(int acNo) {
		this.acNo = acNo;
	}
	public int getEpId() {
		return epId;
	}
	public void setEpId(int epId) {
		this.epId = epId;
	}
	public int getSectionCount() {
		return sectionCount;
	}
	public void setSectionCount(int sectionCount) {
		this.sectionCount = sectionCount;
	}
	public String getVoterCount() {
		return voterCount;
	}
	public void setVoterCount(String voterCount) {
		this.voterCount = voterCount;
	}
	public int getNewSectionCount() {
		return newSectionCount;
	}
	public void setNewSectionCount(int newSectionCount) {
		this.newSectionCount = newSectionCount;
	}
	public int getDetailsUpdated() {
		return detailsUpdated;
	}
	public void setDetailsUpdated(int detailsUpdated) {
		this.detailsUpdated = detailsUpdated;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public double getWorkdone() {
		return workdone;
	}
	public void setWorkdone(double workdone) {
		this.workdone = workdone;
	}
	
	
	

}
